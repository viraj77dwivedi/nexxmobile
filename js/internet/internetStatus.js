'use strict';
angular
    .module('NexxApp')
    .factory('internetStatus',
    [
        function () {
            return {
                check: check
            };
            function check() {
                if (!!window.Connection) {
                    return !!(window.Connection && (navigator.connection.type !== Connection.NONE && navigator.connection.type !== Connection.UNKNOWN));
                } else return !window.cordova;
            }
        }
    ]);