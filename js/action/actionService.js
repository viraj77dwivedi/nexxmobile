angular.module('NexxApp.action')

.factory('actionService',[ '$http',
                            '$q',
                            'internetStatus', 
                            'loadingService',
                            'API_URI',
                            '$timeout',
                            function($http,
                                     $q,
                                     internetStatus,
                                     loadingService,
                                     API_URI,
                                     $timeout
                            ) {
  
              return {
                 action: action,
              }
         

              function action(data) {
                var deferred = $q.defer();
                if (internetStatus.check()) {
                     loadingService.show();
                    $http.get('js/mockup-data/action-list.json').success(function (response, status, headers, config) {
                        $timeout(function () {
                             loadingService.hide();
                             deferred.resolve(response, status, headers, config);
                        }, 2000);     
                    }).error(function (response, status, headers, config) {
                          $timeout(function () {
                             loadingService.hide();
                            deferred.reject(response, status, headers, config);
                         }, 2000);     
                    });
                } else {
                    deferred.reject("No internet connection");
                }
                return deferred.promise;
            }
           
}]);
