angular.module('NexxApp.action', [])

.controller('ActionCtrl',["$scope",
                       "actionService", 
                      function($scope,
                               actionService
               ) {

  	         $scope.data = {};
               $scope.IsVisible = false;
               $scope.togglaButton = function () {
                    $scope.IsVisible = $scope.IsVisible ? false : true;
              };

                $scope.actionLoad = function () {
                   actionService
                    .action($scope.data)
                    .then(
                        function (response, status, headers, config) {
                              $scope.actionData = response.actions;
                            //  alert(JSON.stringify($scope.actionData));
                        },
                        function (response, status, headers, config) {
                              console.log(response);
                        }
                    );
            };
            
}]);


