'use strict';

angular
    .module('NexxApp')
    .factory('loadingService',
    [
        '$ionicLoading',
        function ($ionicLoading) {

            return {
                show: show,
                hide: hide
            };

            function show(duration) {
                var params = {
                    template: '<ion-spinner></ion-spinner>'
                };

                if (!!duration) {
                    params.duration = duration;
                }
                $ionicLoading.show(params);
            }

            function hide() {
                $ionicLoading.hide();
            }
        }
    ]);