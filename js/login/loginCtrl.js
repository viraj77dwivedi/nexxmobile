angular.module('NexxApp.login', [])

.controller('LoginCtrl',["$scope",
                       "loginService", 
                       "$state",
                      function($scope,
                               loginService,
                               $state
                      ) {

	          $scope.data = {};
              $scope.doLogin = function () {
                loginService
                    .login($scope.data)
                    .then(
                        function (response, status, headers, config) {
                               $state.go('tab.action');
                              console.log(response);
                        },
                        function (response, status, headers, config) {
                               $state.go('tab.action');
                              console.log(response);
                        }
                    );
            };
	
}]);


