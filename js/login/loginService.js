angular.module('NexxApp.login')

.factory('loginService',[ '$http',
                            '$q',
                            'internetStatus', 
                            'loadingService',
                            'API_URI',
                            '$timeout',
                            function($http,
                                     $q,
                                     internetStatus,
                                     loadingService,
                                     API_URI,
                                     $timeout
                            ) {
  
            return {
              login: login,
            }
          /**
            *
            * @param email
            * @param password
          */
              function login(data) {
                var deferred = $q.defer();
                if (internetStatus.check()) {
                     loadingService.show();
                    $http.post("APP_URI", {
                        _email: data.email,
                        _password: data.password
                      }).success(function (response, status, headers, config) {
                        $timeout(function () {
                             loadingService.hide();
                             deferred.resolve(response, status, headers, config);
                        }, 2000);     
                    }).error(function (response, status, headers, config) {
                          $timeout(function () {
                             loadingService.hide();
                            deferred.reject(response, status, headers, config);
                         }, 2000);     
                    });
                } else {
                    deferred.reject("No internet connection");
                }
                return deferred.promise;
            }

}]);
