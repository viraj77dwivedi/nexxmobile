angular.module('NexxApp.signup')

.factory('signupService',[ '$http',
                            '$q',
                            'internetStatus', 
                            'loadingService',
                            'API_URI',
                            '$timeout',
                            function($http,
                                     $q,
                                     internetStatus,
                                     loadingService,
                                     API_URI,
                                     $timeout
                            ) {
  
            return {
              register: register,
            }
         

            function register(data) {
                var deferred = $q.defer();
                if (internetStatus.check()) {
                     loadingService.show();
                    $http.post("APP_URI", {
                        param1: data.firstname,
                        param2: data.lastname,
                        param3: data.timezone,
                        param4: data.email,
                        param5: data.password,
                        param6: data.confirmpassword,
                      }).success(function (response, status, headers, config) {
                              loadingService.hide();
                            deferred.resolve(response, status, headers, config);
                        }).error(function (response, status, headers, config) {
                             loadingService.hide();
                            deferred.reject(response, status, headers, config);
                        });
                } else {
                    deferred.reject("No internet connection");
                }
                return deferred.promise;

            }

            

}]);
