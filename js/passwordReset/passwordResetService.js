angular.module('NexxApp.passwordReset')

.factory('PasswordResetService',[ '$http',
                            '$q',
                            'internetStatus', 
                            'loadingService',
                            'API_URI',
                            '$timeout',
                            function($http,
                                     $q,
                                     internetStatus,
                                     loadingService,
                                     API_URI,
                                     $timeout
                            ) {
  
            return {
              passwordReset:passwordReset
            }
         
              function passwordReset(data) {
                var deferred = $q.defer();
                if (internetStatus.check()) {
                     loadingService.show();
                    $http.post("APP_URI", {
                        param1: data.code,
                        param2: data.password,
                        param3: data.confirmpassword,
                      }).success(function (response, status, headers, config) {
                              loadingService.hide();
                            deferred.resolve(response, status, headers, config);
                        }).error(function (response, status, headers, config) {
                             loadingService.hide();
                            deferred.reject(response, status, headers, config);
                        });
                } else {
                    deferred.reject("No internet connection");
                }
                return deferred.promise;
            }

}]);
