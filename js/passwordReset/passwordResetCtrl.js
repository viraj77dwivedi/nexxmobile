angular.module('NexxApp.passwordReset', [])

.controller('PasswordResetCtrl',["$scope",
                       "PasswordResetService", 
                      function($scope,
                               PasswordResetService
                      ) {
  	            $scope.data = {};
                $scope.doReset = function () {
                PasswordResetService.passwordReset($scope.data).then(
                    function (response, status, headers, config) {
                        console.log(response);
                    },
                    function (response, status, headers, config) {
                        console.log(response);
                    }
                );
            };
}]);